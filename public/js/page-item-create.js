//DOM Ready
(function($) {
$(document).ready(function()
{    
	var activeWell = $('#item_create_tab_global');

	var ItemCreateLoader = $('#item_create_loader');
	var ItemCreateNav = $('#item_create_nav');
	var ItemCreateForm = $('#item_create_form');

	var ItemCreateGlobalClass = $('#item_create_global_class');
	var ItemCreateGlobalSubclass = $('#item_create_global_subclass');
	var ItemCreateGlobalInventorytype = $('#item_create_global_inventorytype');
	var ItemCreatePrerequisAllowableClass = $('#item_create_prerequis_allowableclass');
	var ItemCreatePrerequisAllowableRace = $('#item_create_prerequis_allowablerace');

	var InputClass = $('input[name=class]');

	//=====================
	//Navigation ==========
	//=====================

	//onclick
	itemCreateTabClick = function(item)
	{
		var href = item.attr('href'); //get current link
		var tab = href.substr(5);

		item.nextAll('.active').removeClass('active'); //remove class active
        item.prevAll('.active').removeClass('active'); //remove class acive

        switch(tab)
        {
        	//supported tab
        	case 'global':
        	case 'prerequis':
        	case 'stats': 
        	case 'damage':
        	case 'resistances':
        	case 'spells':
        	case 'gem':
        	case 'advanced':
        		itemCreateToggleListWell($('#item_create_tab_'+tab)); 
        		break;

        	default: 
        		itemCreateToggleListWell($('#item_create_tab_global')); 
        		break;
        }

        item.addClass('active');
        sylvanasHelper.setGetParameters('tab', tab, 'global');
        return false;
	}

	//wide show a well
	itemCreateToggleListWell = function(item)
	{
		if(activeWell === item)
			return;

		activeWell
			.addClass('animated')
			.removeClass('fadeInRight')
			.addClass('fadeOutRight')
			.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function()
          	{
          		$(this).hide();

            	item
					.addClass('animated')
					.removeClass('fadeOutRight')
					.addClass('fadeInRight')
					.show();
            })

        activeWell = item;
	}

	//============================================
	//Class ======================================
	//============================================

	var load = true;
	clickOnClass = function(item)
	{
		//remove active of all button
		item.parent().parent().find('button')
			.removeClass('btn-primary')
			.addClass('btn-default');

		//set this active
		item
			.removeClass('btn-default')
			.addClass('btn-primary');

		var itemClassId = item.attr('value');
		InputClass.val(itemClassId);
		sylvanasHelper.setGetParameters("class", itemClassId, "");

		sylvanasApi.getItemSubclass(animatedFadeOut(ItemCreateGlobalSubclass), itemClassId).done(function(data)
		{
			var dom = generateDomItemSubclass(data);

			ItemCreateGlobalSubclass.empty();
			ItemCreateGlobalSubclass.append(dom);

			if(load === true) //when is load we need to click on the button
			{
				var paramSubclass =  $.url().param('subclass');

				if(paramSubclass)
					ItemCreateGlobalSubclass.find('button[value='+paramSubclass+']').click();
				else
					ItemCreateGlobalSubclass.find('button:first').click();

				load=false;
			}
			else
				ItemCreateGlobalSubclass.find('button:first').click();

			animatedFadeIn(ItemCreateGlobalSubclass);
		});

		return false;
	}

	//============================
	//Select2 ====================
	//============================

	sylvanasSelect2.createDisplayInfoSelect2($('input[name=displayid]'));
	sylvanasSelect2.createQualitySelect2($('input[name=Quality]'));

	//=================== 
	//Loader ============
	//===================

	//load all params from url
	loadParams = function()
	{
		var params = $.url().param();

		$.each(params, function(index, value)
		{
			//naviguation click
			//don't nee do rest
			if(index === 'tab')
			{
				var tabLink = ItemCreateNav.find('a[href="?tab='+value+'"]');
				tabLink.click();
				return true;
			}

			//simple select2 set the value
			var input = $('input[name='+index+']');
			if(input !== null)
			{
				input.val(value).change();
			}
		});
	}

	//end load
	loadEnded = function()
	{
		ItemCreateLoader
			.addClass('animated')
			.addClass('fadeOut')
			.hide();

		ItemCreateNav
			.addClass('animated')
			.addClass('fadeIn')
			.show();
	}
	

	//==========================================================================================

	//load item class
	sylvanasApi.getItemClass(animatedFadeOut(ItemCreateGlobalClass)).done(function(data)
	{
		var dom = generateDomItemClass(data);
		getSelectButtons(dom, ItemCreateGlobalClass, 'class');

		animatedFadeIn(ItemCreateGlobalClass);
	});

	//load inventory type
	sylvanasApi.getItemInventorytype(animatedFadeOut(ItemCreateGlobalInventorytype)).done(function(data)
	{
		var dom = generateDomItemInventorytype(data);
		getSelectButtons(dom, ItemCreateGlobalInventorytype, 'InventoryType');

		animatedFadeIn(ItemCreateGlobalInventorytype);
	});

	//load characters classes
	sylvanasApi.getCharactersClasses(animatedFadeOut(ItemCreatePrerequisAllowableClass)).done(function(data)
	{
		var dom = generateDomItemAllowableClass(data);

		ItemCreatePrerequisAllowableClass.empty();
        ItemCreatePrerequisAllowableClass.append(dom);

        animatedFadeIn(ItemCreatePrerequisAllowableClass);
	});

	sylvanasApi.getCharactersRaces(animatedFadeOut(ItemCreatePrerequisAllowableRace)).done(function(data){
		var dom = generateDomItemAllowableRace(data);

		ItemCreatePrerequisAllowableRace.empty();
		ItemCreatePrerequisAllowableRace.append(dom);

		animatedFadeIn(ItemCreatePrerequisAllowableRace);
	});

	//init all tooltips
	$('.tooltips').tooltip();

	//input change set get params
	$( 'input' ).change(function() 
	{
		var inputName = $(this).attr('name');
		if(inputName !==)
			return;

		var inputValue = $(this).val();
		sylvanasHelper.setGetParameters(inputName, inputValue, '');
	});	
	
	loadParams(); //load page get param
	loadEnded(); //end of the load
});

})(jQuery);