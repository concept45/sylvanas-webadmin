sylvanasHelper = function()
{
    var defaultPageTitle = 'Sylvanas :: ';

    var pageContent =  $('#page-content'); //selector of page-content layout
    var pageSpinner = $('#page-spinner'); //select of page-spinner layout
    var pageParameters = Array();


    //================================
    //Private ========================
    //================================

    //helper
    //navigation in ajax
    var beforeUpdatePageContent = function(purge)
    {
        //save the parameters
        if(purge !== false)
          pageParameters[$.url().attr('directory')] = $.url().param();
        else
          delete pageParameters[$.url().attr('directory')];

         //hide content
         pageContent
             .removeClass('fadeIn')
             .removeClass('fadeOut')
             .addClass('fadeOut')
             .hide();

         //show spinner
         pageSpinner
             .removeClass('fadeIn')
             .addClass('fadeIn')
             .show();
    };

    var afterUpdatePageContent = function(data, target)
    {
        //change the url
        var KeyPageParameters = $.url(target).attr('directory');
        var StringParams = "";

        if(pageParameters[KeyPageParameters] != null)
          StringParams = $.param(pageParameters[KeyPageParameters]);

        if(StringParams.length !== 0)
          window.history.pushState('URL','',target+"?"+StringParams);
        else
          window.history.pushState('URL','',target);

        //check data
        if(data.hasOwnProperty("page_title") && data.hasOwnProperty("html"))
        {
            document.title = defaultPageTitle + data["page_title"];

            pageContent.empty(); //wipe the content
            pageContent.append(data["html"]); //add the content
        }
        else //Something wrong
        {
        }

        pageSpinner
            .removeClass('fadeIn')
            .addClass('fadeOut')
            .hide();

        //show content
        pageContent
         .removeClass('fadeOut')
         .addClass('fadeIn')
         .show();
    };

     //purge clear page parameters
    var updatePageContent = function(target, purge)
    {
        var KeyPageParameters = $.url(target).attr('directory');
        console.log(KeyPageParameters);
        return requestAjax(target, pageParameters[KeyPageParameters], beforeUpdatePageContent(purge));
    };

    //===================================
    //Public ============================
    //===================================

    //== Naviguation ================

    //event click
    navClick = function(item)
    {
        var href = item.attr('href'); //Get current link
        var purge = item.attr('purge') || false; 
       
        item.parent().siblings().removeClass('active'); //remove class acive
        item.closest('li').addClass('active'); //add class active

        updatePageContent(href, purge).done(function(data)
        {
            afterUpdatePageContent(data, href);
        }); 

        return false;
    };

    //================================================

    //animate-css helper
    var animate = function(animationAdd, animationRemove, container, callback)
    {
        container
            .addClass('animated')
            .removeClass(animationRemove)
            .addClass(animationAdd)
            .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function()
            {
                if(callback)
                    callback;
            });
    };

    //update dom helper
    var updateDOM = function(container, data)
    {
        container
          .addClass('animated')
          .removeClass('fadeIn')
          .addClass('fadeOut')
          .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function()
          {
               //append the data
               container.empty();
               container.append(data);
               animate('fadeOut', 'fadeIn', container, null);
          });
    };

    //get parameters add helper
    var setGetParameters = function(params_name, value, removeValue)
    {
        //token protect
        if(params_name === "_token")
            return;

        params = $.url().param();

        if(value !== null && value !== removeValue)
            params[params_name] = value;
        else
            delete params[params_name];

        var StringParams = $.param(params);
        if(StringParams.length != 0)
            window.history.pushState('URL','', $.url().attr('directory')+"?"+StringParams);
        else
            window.history.pushState('URL','', $.url().attr('directory'));
    };

    //ajax helper
    var  requestAjax = function( url, params, beforeCallBack)
    {
        return $.ajax({
              url: url,
              data: params,
              beforeSend : beforeCallBack,
              async: true,
              dataType: 'json',
         });
    };

    //checkbox mask helper
    varcheckedCheckbox = function(checkbox, inputName, getParams, allValue)
    {
      //logical is the same
      var checkboxValue = parseInt(checkbox.val());
      var input =  $('input[name='+inputName+']');
      var isChecked = checkbox.is(':checked');

      if(checkboxValue === allValue)
      {
        if(isChecked === true)
        {
          var Allcheckbox = checkbox.parents('.checkbox-group').find('input:checkbox').not(checkbox);
          Allcheckbox.prop('checked', false);

          input.val(allValue);
          setGetParameters(getParams, allValue, '');
        }
        else //if is uncheck
        {
          input.val('');
          setGetParameters(getParams, '', '');
        }
        
      }
      else //allowed value
      {
        var checkboxAll = checkbox.parents('.checkbox-group').find('input:checkbox[value='+allValue+']');

        //uncheck special checkbox
        if(checkboxAll !== null && checkboxAll.is(':checked'))
          checkboxAll.prop('checked', false);

        var inputValue = -1;

        if(input !== null && $.isNumeric(input.val()) && input.val() !== '') //get the input value
          inputValue = parseInt(input.val());

        var finalValue = 0; //final value , when all is uncheckd is 0
        if(inputValue !== -1) //input value have flag
        {
          if(isChecked)
            finalValue = inputValue | checkboxValue;
          else
            finalValue = inputValue & ~checkboxValue;
        }
        else
        {
          finalValue = checkboxValue;
        }
          

        setGetParameters(getParams, finalValue, 0);
        input.val(finalValue);
      }
    };

    //============================
    //Forms=======================
    //============================

    var clickOnSelectButtons = function(key, button, input)
    {
      button.parent().parent().find('button')
        .removeClass('btn-primary')
        .addClass('btn-default');

      button
        .removeClass('btn-default')
        .addClass('btn-primary');

      var buttonValue = button.attr('value');

      input.val(buttonValue);
      setGetParameters(key, buttonValue, "");
      return false;
    };

    var getSelectButtons = function(dom, dom_container, param_name)
    {
        dom_container.empty();
        dom_container.append(dom);

        var param = $.url().param(param_name);

        if(param !== null)
            dom_container.find('button[value='+param+']').click();
        /*else
            dom_container.find('button:first').click();*/
    };

    //*****************************************************

    return {
        requestAjax : requestAjax,
        setGetParameters : setGetParameters,
        updateDOM : updateDOM,
        animate : animate,
        navClick : navClick,
    };

}();