//DOM Ready
(function($) {
$(document).ready(function()
{    
    //=== selector

    //list
    var itemList = $('#item_list');
    var itemListAlert = $('#item_list_alert');

    //count
    var itemCount = $('#item_count');
    var customItemCount = $('#custom_item_count');

    //item
    var tableItemCount = $('#item_list_count');
    var tableItemCountTotal = $('#item_list_count_total');
    var spinnerItemTable = $('#item_list_spinner'); //spinner selector
    var bodyItemTable = $('#item_list_table_body'); //data table selector
    
    //search
    var itemSearch = $('#item_search');
    var itemSearchFormInput = $('#item_form_input');
    var itemSearchForm = $('#item_search_form');
    var itemSearchButton = $('#item_search_button');

    //msg no result
    var alertNoResult = '<div class="alert alert-dismissable alert-danger">\
                        <strong>Oh mince!</strong> Nous n\'avons rien trouvé. Changer les filtres et essayer de les réappliquer.\
                        </div>';



    //get params
    var page = $.url().param('page'); //page

    if(page === null || page === '' || page === undefined || !$.isNumeric(page))
        page = 1;

    var showFilter = $.url().param('showfilter'); //page
    var filter = null;
    var itemSearchShow = false;

    //=================
    //Function ========
    //=================

    //result
    noResult = function()
    {
        hideItemList(true);

        itemListAlert.empty();
        itemListAlert.append(alertNoResult);

        bodyItemTable.empty();

        showItemList(true);

        //pagination
        $('.item_list_pagination').empty();
        $('.item_list_pagination').removeData('twbs-pagination');
        $('.item_list_pagination').unbind('page');
    }


    //==================================
    //Item count =======================
    //==================================


    //===========================
    //Filter ====================
    //===========================

    //hide/show filter div
    itemSearchToggle= function(statut)
    {
        //statut not set
        //itemSearchShow
        if(statut === null)
            itemSearchShow = !itemSearchShow;
        else
            itemSearchShow = statut;

        //apply the get parameter
        sylvanasHelper.setGetParameters('showfilter', itemSearchShow, false);

        if(!itemSearchShow) //hide
        {
            itemSearchButton.empty()
                .append('Afficher')
                .removeClass('btn-danger')
                .addClass('btn-success');

            itemSearch
                .removeClass('fadeInDown')
                .addClass('fadeOutUp')
                .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function()
                {
                    $(this).hide();
                });
        }
        else //show
        {
            itemSearchButton.empty()
                .append('Cacher')
                .removeClass('btn-success')
                .addClass('btn-danger');

            itemSearch
                .removeClass('fadeOutUp')
                .addClass('fadeInDown')
                .show();
        }
    }

    applyFilter = function()
    {
        page = 1;
        itemSearchForm.submit();
    }

    deleteFilters = function()
    {
        $("input").each(function()
        {
            $(this).val("");
        });
        
        itemSearchForm.submit();
    }

    //===========================
    //Item table ================
    //===========================


    //hide the item list
    hideItemList = function(first)
    {
        if(first)
        {
            spinnerItemTable
                .addClass('animated')
                .removeClass('fadeOut')
                .addClass('fadeIn')
                .show();

            itemList
                .addClass('animated')
                .removeClass('fadeIn')
                .addClass('fadeOut')
        }
        else
        {
            bodyItemTable
                .addClass('animated')
                .removeClass('fadeInLeft')
                .addClass('fadeOutRight');
        }
    }

    //show the item list
    showItemList = function(first)
    {
        if(first)
        {
            //hide the spinner
            spinnerItemTable
                .addClass('animated')
                .removeClass('fadeIn')
                .addClass('fadeOut')
                .hide()

            //show the table
            itemList
                .addClass('animated')
                .removeClass('fadeOut')
                .addClass('fadeIn')
        }
        else
        {
            bodyItemTable
                .addClass('animated')
                .removeClass('fadeOutRight')
                .addClass('fadeInLeft')
        }
    }

    beforeGetPage = function(first)
    {
       hideItemList(first);
    }

    afterGetPage = function(data, first)
    {
        //table 
        sylvanasHelper.updateDOM(tableItemCount, data.from+'-'+data.to);

        if(first)
            sylvanasHelper.updateDOM(tableItemCountTotal, data.total);

        if(data['data'].length === 0)
        {
            noResult();
            return;
        }

        itemListAlert.empty();

        var dom = getDOMItemTable(data['data']);
        bodyItemTable.empty();
        bodyItemTable.append(dom);

        showItemList(first);

        if(first)
        {
            //pagination delete
            $('.item_list_pagination').empty();
            $('.item_list_pagination').removeData('twbs-pagination');
            $('.item_list_pagination').unbind('page');

            //pagination
            $('.item_list_pagination').twbsPagination({
                totalPages: data.last_page,
                startPage: data.current_page,
                first: '<i class="fa fa-step-backward"></i>',
                prev : '<i class="fa fa-chevron-left"></i>',
                next : '<i class="fa fa-chevron-right"></i>',
                last : '<i class="fa fa-step-forward"></i>',
                visiblePages: 15,

                onPageClick: function (event, page) 
                {
                    sylvanasApi.getItem(page, filter, beforeGetPage(false)).done(function(data)
                    {
                        afterGetPage(data, false)
                    });
                }
            });
        }
    }

    //=========================
    //WHEN PAGE IS LOADED =====
    //=========================

    //show hide the itemSearch
    itemSearchToggle(showFilter);

    //load the filter list
    sylvanasApi.getItemSearchFilter(null).done(function(data)
    {
        var dom = itemSearchGenerateDOMForm(data);
        itemSearchFormInput.append(dom);

        filter = itemSearchForm.serializeArray();

        //load the page parameter
        sylvanasApi.getItem(page, filter, beforeGetPage(true)).done(function(data)
        {
            afterGetPage(data, true);
        });
    });

    //intercept submit
    itemSearchForm.submit(function(e)
    {
        e.preventDefault();

        filter = $(this).serializeArray();
        
        applyItemSearchFilter(filter, page, beforeGetPage(true)).done(function(data)
        {
            afterGetPage(data, true);
        }); 
    })

    //count item in database
    sylvanasApi.getItemCount().done(function(data)
    {
         if(data.hasOwnProperty('count'))
         {
             var count = data['count'];
             sylvanasHelper.updateDOM(itemCount, count);
         }
    });

    
});

})(jQuery);