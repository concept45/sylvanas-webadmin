DROP TABLE IF EXISTS `character_classes`;

CREATE TABLE `character_classes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `libelle` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

insert  into `character_classes`(`id`,`libelle`) values (1,'Guerrier'),(2,'Paladin'),(3,'Chasseur'),(4,'Voleur'),(5,'Pr�tre'),(6,'Chevalier de la mort'),(7,'Chaman'),(8,'Mage'),(9,'D�moniste'),(10,'Druide');
