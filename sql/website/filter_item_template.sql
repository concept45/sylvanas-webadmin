DROP TABLE IF EXISTS `filter_item_template`;

CREATE TABLE `filter_item_template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `column_name` text NOT NULL,
  `libelle` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `filter_item_template` */

insert  into `filter_item_template`(`id`,`column_name`,`libelle`) values (1,'entry','Entry'),(2,'name\r\n','Nom');

