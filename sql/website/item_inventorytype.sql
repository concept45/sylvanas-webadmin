DROP TABLE IF EXISTS `item_inventorytype`;

CREATE TABLE `item_inventorytype` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `libelle` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

insert  into `item_inventorytype`(`id`,`libelle`) values (0,'Non �quipable'),(1,'T�te'),(2,'Cou'),(3,'Epaule'),(4,'Chemise'),(5,'Poitrine'),(6,'Taille'),(7,'Jambes'),(8,'Pieds'),(9,'Poignets'),(10,'Mains'),(11,'Doigt'),(12,'Bibelot'),(13,'Arme'),(14,'Bouclier'),(15,'� distance (Arcs)'),(16,'Dos'),(17,'Deux mains'),(18,'Sac'),(19,'Tabard'),(20,'Robe'),(21,'Main droite'),(22,'Main gauche'),(23,'Pouvant �tre tenu (Tome)'),(24,'Munition'),(25,'Arme de jet'),(26,'� distance droite (Baguettes, Fusils)'),(27,'Carquois'),(28,'Relique');
