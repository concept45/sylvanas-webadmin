DROP TABLE IF EXISTS `character_races`;

CREATE TABLE `character_races` (
  `id` int(10) unsigned NOT NULL,
  `libelle` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert  into `character_races`(`id`,`libelle`) values (1,'Humain'),(2,'Orc'),(3,'Nain'),(4,'Elfe de la nuit'),(5,'Mort-vivant'),(6,'Tauren'),(7,'Gnome'),(8,'Troll'),(9,'Gobelin'),(10,'Elfe de sang'),(11,'Draeneï'),(12,'Gangr\'orc'),(13,'Naga'),(14,'Roué'),(15,'Squelette'),(16,'Vrykul'),(17,'Rohart'),(18,'Troll des forêts'),(19,'Taunka'),(20,'Squelette du Norfendre'),(21,'Troll des glaces');
