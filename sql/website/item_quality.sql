DROP TABLE IF EXISTS `item_quality`;

CREATE TABLE `item_quality` (
  `id` int(10) unsigned NOT NULL,
  `libelle` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert  into `item_quality`(`id`,`libelle`) values (0,'[Gris] Qualit� basse'),(1,'[Blanc] Commun'),(2,'[Vert] Non commun'),(3,'[Bleu] Rare'),(4,'[Violet] Epic'),(5,'[Orange] L�gendaire'),(6,'[Rouge] Art�fact'),(7,'[Or] Li� au compte');
