DROP TABLE IF EXISTS `item_class`;

CREATE TABLE `item_class` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `libelle` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

/*Data for the table `item_class` */

insert  into `item_class`(`id`,`libelle`) values (1,'Consommable'),(2,'Arme'),(3,'Gemme'),(4,'Armure'),(5,'Composant'),(6,'Projectile'),(7,'Artisanat'),(8,'Générique(OBSOLETE)'),(9,'Recette'),(10,'Argent'),(11,'Carquois'),(12,'Quête'),(13,'Clé'),(14,'Permanent(OBSOLETE)'),(15,'Divers'),(16,'Glyphe');
