<div class="page-header clearfix">
    <div class="row">
      <div class="col-lg-6">
        <h1>Gestions des objets</h1>
      </div>
      <div class="col-lg-6" id="item_index_infos">
          @include('pages.item.partials.index.infos')
      </div>    
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
      @include('pages.item.partials.index.search')
    </div>
</div>
<hr/>
<div class="row">
    <div class="col-lg-12">
        @include('pages.item.partials.index.list')
    </div>
</div>


{{-- JS Script --}}
@section('pageScript')

  @include('js.item')
  @include('js.item_search')

  {{-- Script de la page--}}
  {{ HTML::script('js/page-item.js'); }}
  
@stop

