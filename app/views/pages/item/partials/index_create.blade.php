<div class="page-header clearfix">
    <div class="row">
      <div class="col-lg-12">
        <h1>Nouveau objet <small id="create_model"></small>
          <span class="pull-right">
              <button class="btn btn-success"><i class="fa fa-upload"></i> Sauvegarder</button> <button href="{{URL::route('item.index.create');}}" class="btn btn-warning" onclick="return navClick($(this), true)"><i class="fa fa-warning"></i> Nouveau</button>
          </span>
        </h1>
      </div>
    </div>
</div>
<div class="text-center" id="item_create_loader">
    <i class="fa fa-2x fa-cog fa-spin"></i>
</div>
<div class="row" id="item_create_nav" hidden>
  <div class="col-lg-3">
  	<div class="list-group">
		  <a href="?tab=global" class="list-group-item active" onclick="return itemCreateTabClick($(this))">Général</a>
		  <a href="?tab=prerequis" class="list-group-item" onclick="return itemCreateTabClick($(this))">Prérequis</a>
		  <a href="?tab=stats" class="list-group-item" onclick="return itemCreateTabClick($(this))">Stats</a>
		  <a href="?tab=damage" class="list-group-item" onclick="return itemCreateTabClick($(this))" >Dégats</a>
		  <a href="?tab=resistances" class="list-group-item" onclick="return itemCreateTabClick($(this))">Résistances</a>
		  <a href="?tab=spells" class="list-group-item" onclick="return itemCreateTabClick($(this))">Sorts</a>
		  <a href="?tab=gem" class="list-group-item" onclick="return itemCreateTabClick($(this))">Gemmes</a>
		  <a href="?tab=advanced" class="list-group-item" onclick="return itemCreateTabClick($(this))">Avancés/Autres</a>
	 </div>
  </div>
  <div class="col-lg-9" >
  	<form role="form" id="item_create_form">
  		<div class="well well-lg" id="item_create_tab_global">
  			@include('pages.item.partials.create.global')
        </div>
        <div class="well well-lg" id="item_create_tab_prerequis" hidden>
        	@include('pages.item.partials.create.prerequis')
        </div>
        <div class="well well-lg" id="item_create_tab_stats" hidden>
        	@include('pages.item.partials.create.stats')
        </div>
        <div class="well well-lg" id="item_create_tab_damage" hidden>
        	@include('pages.item.partials.create.damage')
        </div>
        <div class="well well-lg" id="item_create_tab_resistances" hidden>
        	@include('pages.item.partials.create.resistances')
        </div>
        <div class="well well-lg" id="item_create_tab_spells" hidden>
        	@include('pages.item.partials.create.spells')
        </div>
        <div class="well well-lg" id="item_create_tab_gem" hidden>
        	@include('pages.item.partials.create.gem')
        </div>
        <div class="well well-lg" id="item_create_tab_advanced" hidden>
        	@include('pages.item.partials.create.advanced')
        </div>
    </form>
  </div>
</div>

@section('pageScript')

 {{-- JS Script --}}
  @include('js.item_create')

  {{-- Page JS Script --}}
  {{ HTML::script('js/page-item-create.js'); }}

@stop