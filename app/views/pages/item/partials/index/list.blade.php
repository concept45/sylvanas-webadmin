<div id="item_list">
    <div class="row">
    <div class="col-lg-6 col-lg-offset-3" id="item_list_alert">
        </div>
    </div>
    <h1>Liste des objets 
    <small id="item_list_count"></small>

    <small class="pull-right"><span id="item_list_count_total"></span> Objets</small>
    </h1>
    <div id="item_list_spinner" hidden>
        <div class="progress progress-striped active">
            <div class="progress-bar" style="width: 100%"></div>
        </div>
    </div>
    @include('pages.item.partials.index.list_pagination')
    <table class="table table-striped table-hover tablesorter" id="item_list_table">
        <thead>
          <tr>
            <th>#</th>
            <th>{{-- Icon --}}</th>
            <th>Nom</th>
            <th>Nom Fr-fr</th>
            <th>Type</th>
            <th>Niveau</th>
            <th></th>
          </tr>
        </thead>
        <tbody id="item_list_table_body">
        </tbody>
    </table>
    @include('pages.item.partials.index.list_pagination')
</div>