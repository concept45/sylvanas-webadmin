<h3>
	Filtre (&)
	<span class="pull-right">
		<button class="btn btn-success btn-sm" id="item_search_button" onclick="itemSearchToggle(null)">Afficher</button>
	</span>
</h3>
<div class="animated" id="item_search" hidden>
	<form id="item_search_form" class="form-horizontal" role="form" method="post">
		<span id="item_form_input"></span>
		{{-- Boutons --}}
		<div class="row">
			<div class="col-lg-12 text-right">
				 <a class="btn btn-info" target="_blank" href="http://collab.kpsn.org/display/tc/item_template">Documentation TrinityCore</a>
                 <a class="btn btn-danger" onclick="deleteFilters();return false;">Supprimer les filtres</a>
                 <button type="submit" class="btn btn-success" onclick="applyFilter();return false;">Appliquer les filtres</button>
			</div>
		</div>
	</form>
</div>