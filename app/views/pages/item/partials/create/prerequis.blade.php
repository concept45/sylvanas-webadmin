{{-- Les Tooltips doivent être déclaté avant le html (38 char par ligne) --}}

@section('tooltip.requiredlevel')
   Le niveau requis par le joueur<br>
   pour pourvoir utiliser cette objet
@stop

@section('tooltip.requiredskill')
    @TODO
@stop

@section('tooltip.requiredskillrank')
    @TODO
@stop

@section('tooltip.requiredspell')
    @TODO
@stop

@section('tooltip.requiredcityrank')
    @TODO
@stop

@section('tooltip.requiredhonorrank')
    @TODO
@stop

@section('tooltip.requiredreputationfaction')
    @TODO
@stop

@section('tooltip.requiredreputationrank')
    @TODO
@stop

@section('tooltip.allowablerace')
    @TODO
@stop

@section('tooltip.allowableclass')
    @TODO
@stop

@section('tooltip.map')
    @TODO
@stop

@section('tooltip.area')
    @TODO
@stop

{{-- Tooltips end --}}

<h3>Niveau requis</h3><br>
<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">RequiredLevel</div>
              <input class="form-control" name="requiredlevel" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.requiredlevel')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
      </div>
</div>
<hr>
<div class="row">
    <div class="col-lg-6">
        <h3>Classe requise</h3><br>
        <input class="form-control" name="AllowableClass" type="hidden" placeholder="">
        <span class="checkbox-group" id="item_create_prerequis_allowableclass">
        </span>
    </div>
    <div class="col-lg-6">
        <h3>Race requise</h3><br>
        <input class="form-control" name="AllowableRace" type="hidden" placeholder="">
        <span class="checkbox-group" id="item_create_prerequis_allowablerace">
    </div>
</div>
<hr>
<h3>Talent requis</h3><br>
<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">RequiredSkill</div>
              <input class="form-control" name="RequiredSkill" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.requiredskill')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">RequiredSkillRank</div>
              <input class="form-control" name="RequiredSkillRank" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.requiredskillrank')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<hr>
<h3>Sort et/ou honneur requis</h3><br>
<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">requiredspell</div>
              <input class="form-control" name="requiredspell" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.requiredspell')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">requiredhonorrank</div>
              <input class="form-control" name="requiredhonorrank" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.requiredhonorrank')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<hr>
<h3>Rank de ville requis</h3><br>
<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">RequiredCityRank</div>
              <input class="form-control" name="RequiredCityRank" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.requiredcityrank')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<h3>Réputation requise</h3><br>
<div class="row">
   <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">requiredReputationFaction</div>
              <input class="form-control" name="requiredReputationFaction" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.requiredreputationfaction')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">requiredReputationRank</div>
              <input class="form-control" name="requiredReputationRank" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.requiredreputationrank')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<hr>
<h3>Map et/ou Area requis</h3><br>
<div class="row">
   <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">area</div>
              <input class="form-control" name="area" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.area')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">Map</div>
              <input class="form-control" name="map" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.map')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>