{{-- Les Tooltips doivent être déclaté avant le html (38 char par ligne) --}}

@section('tooltip.stat_type')
    @TODO
@stop

@section('tooltip.stat_value')
    @TODO
@stop

@section('tooltip.scalingstatvalue')
    @TODO
@stop

@section('tooltip.scalingstatdistribution')
    @TODO
@stop

@section('tooltip.statscount')
    @TODO
@stop

{{-- Tooltips end --}}

<h3>Statistiques</h3><br>
<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">StatsCount</div>
              <input class="form-control" name="StatsCount" type="text" placeholder="" disabled>
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.statscount')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">ScalingStatValue</div>
              <input class="form-control" name="ScalingstatValue" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.scalingstatvalue')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">ScalingStatDistribution</div>
              <input class="form-control" name="ScalingstatDistribution" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.scalingstatdistribution')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<hr>
<h3>Valeurs des statistiques</h3><br>
@for ($i = 1; $i <= 10; $i++)
<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">stat_type_{{ $i }}</div>
              <input class="form-control" name="stat_type_{{ $i }}" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.stat_type')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">stat_value_{{ $i }}</div>
              <input class="form-control" name="stat_value_{{ $i }}" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.stat_value')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
@endfor