<h3>Flags</h3><br>
<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">Flags</div>
              <input class="form-control" name="Flags" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.flags')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<hr>
<h3>Flags Extra</h3><br>
<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">FlagsExtra</div>
              <input class="form-control" name="FlagsExtra" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.flagsextra')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<hr>
<h3>Flags Custom</h3><br>
<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">FlagsCustom</div>
              <input class="form-control" name="FlagsCustom" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.flagscustom')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<hr>
<h3>Achat et Vente</h3><br>
<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">BuyCount</div>
              <input class="form-control" name="BuyCount" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.buycount')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">BuyPrice</div>
              <input class="form-control" name="BuyPrice" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.buyprice')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">SellPrice</div>
              <input class="form-control" name="SellPrice" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.sellprice')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<hr>
<h3>Inventaire</h3><br>
<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">maxcount</div>
              <input class="form-control" name="maxcount" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.maxcount')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">stackable</div>
              <input class="form-control" name="stackable" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.stackable')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<hr>
<h3>Type de liaison</h3><br>
<div class="row">
	<div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">bonding</div>
              <input class="form-control" name="bonding" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.bonding')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<hr>
<h3>Autres</h3><br>
<div class="row">
	<div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">SoundOverrideSubclass</div>
              <input class="form-control" name="SoundOverrideSubclass" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.soundoverridesubclass')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">ContainerSlots</div>
              <input class="form-control" name="ContainerSlots" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.flags')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">PageText</div>
              <input class="form-control" name="PageText" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.flags')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">LanguageID</div>
              <input class="form-control" name="LanguageID" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.flags')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">PageMaterial</div>
              <input class="form-control" name="PageMaterial" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.flags')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">startquest</div>
              <input class="form-control" name="startquest" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.flags')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">lockid</div>
              <input class="form-control" name="lockid" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.flags')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">Material</div>
              <input class="form-control" name="Material" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.flags')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">sheath</div>
              <input class="form-control" name="sheath" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.flags')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">RandomProperty</div>
              <input class="form-control" name="RandomProperty" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.flags')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">RandomSuffix</div>
              <input class="form-control" name="RandomSuffix" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.flags')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">itemset</div>
              <input class="form-control" name="itemset" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.flags')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">MaxDurability</div>
              <input class="form-control" name="MaxDurability" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.flags')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">BagFamily</div>
              <input class="form-control" name="BagFamily" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.flags')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">TotemCategory</div>
              <input class="form-control" name="TotemCategory" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.flags')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">RequiredDisenchantSkill</div>
              <input class="form-control" name="RequiredDisenchantSkill" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.flags')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">ArmorDamageModifier</div>
              <input class="form-control" name="ArmorDamageModifier" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.flags')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">duration</div>
              <input class="form-control" name="duration" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.flags')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">ItemLimitCategory</div>
              <input class="form-control" name="ItemLimitCategory" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.flags')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">HolidayId</div>
              <input class="form-control" name="HolidayId" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.flags')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">ScriptName</div>
              <input class="form-control" name="ScriptName" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.flags')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">DisenchantID</div>
              <input class="form-control" name="DisenchantID" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.flags')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">FoodType</div>
              <input class="form-control" name="FoodType" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.flags')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">minMoneyLoot</div>
              <input class="form-control" name="minMoneyLoot" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.flags')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">maxMoneyLoot</div>
              <input class="form-control" name="maxMoneyLoot" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.flags')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">WDBVerified</div>
              <input class="form-control" name="WDBVerified" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.flags')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<hr>