{{-- Les Tooltips doivent être déclaté avant le html (38 char par ligne) --}}

@section('tooltip.spellid')
    @TODO
@stop

@section('tooltip.spelltrigger')
    @TODO
@stop

@section('tooltip.spellcharges')
    @TODO
@stop

@section('tooltip.spellppmRate')
    @TODO
@stop

@section('tooltip.spellcooldown')
    @TODO
@stop

@section('tooltip.spellcategory')
    @TODO
@stop

@section('tooltip.spellcategorycooldow')
    @TODO
@stop

{{-- Tooltips end --}}

@for ($i = 1; $i <= 5; $i++)
<h3>Sort #{{$i}}</h3><br>
<div class="row">
	<div class="col-lg-4">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">spellid_{{$i}}</div>
              <input class="form-control" name="spellid_{{$i}}" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.spellid')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">spelltrigger_{{$i}}</div>
              <input class="form-control" name="spelltrigger_{{$i}}" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.spelltrigger')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">spellcharges_{{$i}}</div>
              <input class="form-control" name="spellcharges_{{$i}}" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.spellcharges')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">spellppmRate_{{$i}}</div>
              <input class="form-control" name="spellppmRate_{{$i}}" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.spellppmRate')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<div class="row">
	<div class="col-lg-4">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">spellcooldown_{{$i}}</div>
              <input class="form-control" name="spellcooldown_{{$i}}" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.spellcooldown')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">spellcategory_{{$i}}</div>
              <input class="form-control" name="spellcategory_{{$i}}" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.spellcategory')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">spellcategorycooldown_{{$i}}</div>
              <input class="form-control" name="spellcategorycooldown_{{$i}}" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.spellcategorycooldown')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<hr>
@endfor