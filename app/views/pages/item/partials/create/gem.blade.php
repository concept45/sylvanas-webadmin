{{-- Les Tooltips doivent être déclaté avant le html (38 char par ligne) --}}

@section('tooltip.socketColor')
    @TODO
@stop

@section('tooltip.socketContent')
    @TODO
@stop

@section('tooltip.socketBonus')
    @TODO
@stop

@section('tooltip.GemProperties')
    @TODO
@stop

{{-- Tooltips end --}}

<h3>Gemmes</h3><br>
@for ($i = 1; $i <= 3; $i++)
<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
		    <div class="input-group">
		      <div class="input-group-addon">socketColor_{{$i}}</div>
		      <input class="form-control" name="socketColor_{{$i}}" type="text" placeholder="">
		      <span class="input-group-addon">
		      		<span type="button" class="tooltips"  title="@yield('tooltip.socketColor')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
		      </span>
		    </div>
		</div>
  	</div>
  	<div class="col-lg-6">
		<div class="form-group">
		    <div class="input-group">
		      <div class="input-group-addon">socketContent_{{$i}}</div>
		      <input class="form-control" name="socketContent_{{$i}}" type="text" placeholder="">
 				<span class="input-group-addon">
		      		<span type="button" class="tooltips"  title="@yield('tooltip.socketContent')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
		      	</span>
		    </div>
		</div>
  	</div>
</div>
@endfor
<h3>Bonus et propriétés</h3><br>
<div class="row">
	<div class="col-lg-6">
		<div class="form-group">
		    <div class="input-group">
		      <div class="input-group-addon">socketBonus</div>
		      <input class="form-control" name="socketBonus" type="text" placeholder="">
		      <span class="input-group-addon">
		      		<span type="button" class="tooltips"  title="@yield('tooltip.socketBonus')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
		      </span>
		    </div>
		</div>
  	</div>
  	<div class="col-lg-6">
		<div class="form-group">
		    <div class="input-group">
		      <div class="input-group-addon">GemProperties</div>
		      <input class="form-control" name="GemProperties" type="text" placeholder="">
 				<span class="input-group-addon">
		      		<span type="button" class="tooltips"  title="@yield('tooltip.GemProperties')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
		      	</span>
		    </div>
		</div>
  	</div>
</div>