{{-- Les Tooltips doivent être déclaté avant le html (38 char par ligne) --}}

@section('tooltip.entry')
	Identifiant de votre objet<br>
	Celui-ci est unique<br>
@stop

@section('tooltip.displayid')
	Ce numeros définis l'icone et le model<br>
@stop

@section('tooltip.name')
	Nom de votre objet
@stop

@section('tooltip.description')
	La description de votre objet<br>
	Apparaît en jaune en bas
@stop

@section('tooltip.Quality')
	La qualité de votre objet<br>
	Définis la couleur du texte
@stop

@section('tooltip.ItemLevel')
	Base de comparaison entre 2 objets<br>
@stop

{{-- Tooltips end --}}

<h3>Identifiant et Displayid</h3><br>
<div class="row">
	<div class="col-lg-4">
		<div class="form-group">
		    <div class="input-group">
		      <div class="input-group-addon">entry</div>
		      <input class="form-control" name="entry" type="text" placeholder="">
		      <span class="input-group-addon">
		      		<span type="button" class="tooltips"  title="@yield('tooltip.entry')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
		      </span>
		    </div>
		</div>
  	</div>
  	<div class="col-lg-6">
		<div class="form-group">
		    <div class="input-group select2-bootstrap-prepend select2-bootstrap-append">
		      <div class="input-group-addon">displayid</div>
		      <input class="form-control" name="displayid" type="hidden" placeholder="">
 				<span class="input-group-addon">
		      		<span type="button" class="tooltips"  title="@yield('tooltip.displayid')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
		      	</span>
		    </div>
		</div>
  	</div>
</div>
<hr>
<h3>Nom et description</h3><br>
<div class="row">
	 <div class="col-lg-6">
		<div class="form-group">
		    <div class="input-group">
		      <div class="input-group-addon">name</div>
		      <input class="form-control" name="name" type="text" placeholder="">
		       <span class="input-group-addon">
		      		<span type="button" class="tooltips"  title="@yield('tooltip.name')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
		      </span>
		    </div>
		</div>
  	</div>
  	<div class="col-lg-6">
		<div class="form-group">
		    <div class="input-group">
		      <div class="input-group-addon">description</div>
		      <input class="form-control" name="description" type="text" placeholder="">
		        <span class="input-group-addon">
		      		<span type="button" class="tooltips"  title="@yield('tooltip.description')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
		        </span>
		    </div>
		</div>
  	</div>
</div>
<hr>
<h3>Qualité et niveau d'objet</h3><br>
<div class="row">
	 <div class="col-lg-6">
		<div class="form-group">
		    <div class="input-group select2-bootstrap-prepend select2-bootstrap-append">
		      <span class="input-group-addon">Quality</span>
		       <input class="form-control" name="Quality" type="hidden">
		       <span class="input-group-addon">
		      		<span type="button" class="tooltips"  title="@yield('tooltip.Quality')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
		      </span>
		    </div>
		</div>
  	</div>
  	<div class="col-lg-6">
		<div class="form-group">
		    <div class="input-group">
		      <div class="input-group-addon">ItemLevel</div>
		      <input class="form-control" name="ItemLevel" type="text" placeholder="">
		        <span class="input-group-addon">
		      		<span type="button" class="tooltips"  title="@yield('tooltip.ItemLevel')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
		        </span>
		    </div>
		</div>
  	</div>
</div>
<hr>
<h3>Classe</h3><br>
<div class="row">
	 <input name="class" type="hidden">
	 <div class="col-lg-12" id="item_create_global_class">
  	</div>
</div>
<h3>Sous-classe</h3><br>
<div class="row">
	 <input name="subclass" type="hidden">
	 <div class="col-lg-12" id="item_create_global_subclass">
			<em>Veuillez choisir une classe au préalable</em>
  	</div>
</div>
<h3>Emplacement inventaire</h3><br>
<div class="row">
	 <input name="InventoryType" type="hidden">
	 <div class="col-lg-12" id="item_create_global_inventorytype">
  	</div>
</div>