{{-- Les Tooltips doivent être déclaté avant le html (38 char par ligne) --}}

@section('tooltip.dmg_type')
    @TODO
@stop

@section('tooltip.dmg_min')
    @TODO
@stop

@section('tooltip.dmg_max')
    @TODO
@stop

@section('tooltip.delay')
    @TODO
@stop

@section('tooltip.ammo_type')
    @TODO
@stop

@section('tooltip.rangedmodrange')
    @TODO
@stop


{{-- Tooltips end --}}

<h3>Dégats</h3><br>
@for ($i = 1; $i <= 2; $i++)
<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">dmg_type{{ $i }}</div>
              <input class="form-control" name="dmg_type{{ $i }}" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.dmg_type')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">dmg_min{{ $i }}</div>
              <input class="form-control" name="dmg_min{{ $i }}" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.dmg_min')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">dmg_max{{ $i }}</div>
              <input class="form-control" name="dmg_max{{ $i }}" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.dmg_max')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
@endfor
<div class="row">
    <div class="col-lg-6">
    	<h3>Délai</h3><br>
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">Delay</div>
              <input class="form-control" name="delay" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.delay')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
    	<h3>Type de munition</h3><br>
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">ammo_type</div>
              <input class="form-control" name="ammo_type" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.ammo_type')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
    	<h3>Portée</h3><br>
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">RangedModRange</div>
              <input class="form-control" name="rangedmodrange" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.rangedmodrange')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>