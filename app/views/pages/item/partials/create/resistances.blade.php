{{-- Les Tooltips doivent être déclaté avant le html (38 char par ligne) --}}

@section('tooltip.armor')
    @TODO
@stop

@section('tooltip.block')
    @TODO
@stop

@section('tooltip.holy_res')
    @TODO
@stop

@section('tooltip.fire_res')
    @TODO
@stop

@section('tooltip.nature_res')
    @TODO
@stop

@section('tooltip.frost_res')
    @TODO
@stop

@section('tooltip.shadow_res')
    @TODO
@stop

@section('tooltip.arcane_res')
    @TODO
@stop

{{-- Tooltips end --}}

<h3>Résistances</h3><br>
<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">armor</div>
              <input class="form-control" name="armor" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.armor')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
   </div>
   <div class="col-lg-4">
      <div class="form-group">
          <div class="input-group">
            <div class="input-group-addon">block</div>
            <input class="form-control" name="block" type="text" placeholder="">
            <span class="input-group-addon">
                    <span type="button" class="tooltips"  title="@yield('tooltip.block')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
            </span>
          </div>
      </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">holy_res</div>
              <input class="form-control" name="holy_res" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.holy_res')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">fire_res</div>
              <input class="form-control" name="fire_res" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.fire_res')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">nature_res</div>
              <input class="form-control" name="nature_res" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.nature_res')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-4">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">frost_res</div>
              <input class="form-control" name="frost_res" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.frost_res')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">shadow_res</div>
              <input class="form-control" name="shadow_res" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.shadow_res')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="form-group">
            <div class="input-group">
              <div class="input-group-addon">arcane_res</div>
              <input class="form-control" name="arcane_res" type="text" placeholder="">
              <span class="input-group-addon">
                      <span type="button" class="tooltips"  title="@yield('tooltip.arcane_res')" style="white-space:pre-wrap;" data-html="true" data-toggle="tooltip" data-placement="top"><i class="fa fa-info"></i></span>
              </span>
            </div>
        </div>
    </div>
</div>
