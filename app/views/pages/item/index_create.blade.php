@extends('layouts.default')

@section('navigation')
    @include('includes.nav')
@stop

@section('title')
    {{ isset($page_title) ? $page_title : '' }}
@stop

@section('content')
    @include('pages.item.partials.index_create')
@stop


@section('pageScript')
@stop