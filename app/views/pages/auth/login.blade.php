@extends('layouts.default')

{{-- Pas de navigation --}}
@section('navigation')
@stop

@section('title')
   {{ isset($page_title) ? $page_title : '' }}
@stop

@section('content')
    @include('pages.auth.partials.login')
@stop

@section('pageScript')
    {{ HTML::script('js/auth.js'); }}
@stop