<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <h1 class="text-center">Authentification</h1>
            @if ($success = Session::get('success'))
            <div class="alert alert-dismissable alert-success fade in">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Bravo !</strong> {{ $success }}</a>.
            </div>
            @endif

            @if ($error = Session::get('error'))
            <div class="alert alert-dismissable alert-danger fade in">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>Damm it !</strong><br> {{ $error }}</a>.
            </div>    
            @endif
            <form class="form-signin" method="post">
                <div class="form-group">
                      <input name="username" type="text" class="form-control" placeholder="Username" required autofocus>
                </div>
                <div class="form-group">
                    <input name="password" type="password" class="form-control" placeholder="Password" required>
                </div>
                <div class="form-group checkbox">
                    <label>
                        <input type="checkbox" name="remember-me" value="remember-me">
                        Connexion automatique
                    </label>
                </div>
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
            </form>
        </div>
    </div>
</div>