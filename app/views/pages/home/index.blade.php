@extends('layouts.default')

@section('navigation')
    @include('includes.nav')
@stop

@section('title')
   {{ isset($page_title) ? $page_title : '' }}
@stop

@section('content')
    @include('pages.home.partials.index')
@stop


@section('pageScript')
@stop