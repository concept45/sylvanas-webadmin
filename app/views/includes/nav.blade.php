<div class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
       <div class="navbar-collapse collapse" id="navbar-main">
          <ul class="nav navbar-nav">
                <li class="{{ Route::currentRouteName() == 'home.index' ? 'active' : '' }}">
                  <a href="{{ URL::route('home.index'); }}"  class="navhref" purge="false"><i class="fa fa-home"></i> Accueil</a>
                </li>
                <li class="{{ Route::currentRouteName() == 'item.index' ? 'active' : '' }}">
                  <a href="{{ URL::route('item.index'); }}"  class="navhref" purge="false"><i class="fa fa-bars"></i> Objets</a>
                </li>
                <li class="{{ Route::currentRouteName() == 'item.index.create' ? 'active' : '' }}">
                  <a href="{{ URL::route('item.index.create'); }}" class="navhref" purge="false"><i class="fa fa-plus"></i> Nouveau objet</a>
                </li>
                {{--<li><a href="/">Creatures</a></li>
                <li><a href="/">Gameobjects</a></li>--}}
          </ul>
          <ul class="nav navbar-nav navbar-right">
              <li><a href="{{ URL::route('auth.logout'); }}"><i class="fa fa-power-off"></i> Logout</a></li>
          </ul>
        </div>
    </div>
</div>