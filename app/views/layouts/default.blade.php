<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Sylvanas :: @yield('title')</title> {{-- Title page is also set by Jquery look at global.js --}}

    {{-- Chargement des styles --}}
    {{ HTML::style('bower/bootstrap/dist/css/bootstrap.min.css'); }}
    {{ HTML::style('bower/fontawesome/css/font-awesome.min.css'); }}
    {{ HTML::style('bower/animate-css/animate.min.css'); }}
    {{ HTML::style('bower/select2/select2.css'); }}
    {{ HTML::style('css/select2.bootstrap.css'); }}
    {{ HTML::style('css/bootswatch.min.css'); }}
    {{ HTML::style('css/lumen.bootstrap.min.css'); }}

    {{ HTML::style('css/item.css'); }}
    {{ HTML::style('css/global.css'); }}
    {{ HTML::style('css/spinner.css'); }}
</head>
<body>
    @yield('navigation')
    <div class="container">
        {{-- Le loader de la page --}}
        <div class="spinner" id="page-spinner" hidden></div>
        {{-- Contenue de la page --}}
        <div class="animated fadeIn" id="page-content">
             @yield('content')
        </div>
    </div>
    
    {{-- Tout les scripts bower doivent être chargé ici --}}
    {{ HTML::script('bower/less/dist/less-1.7.4.min.js'); }}
    {{ HTML::script('bower/jquery/dist/jquery.min.js'); }}
    {{ HTML::script('bower/bootstrap/dist/js/bootstrap.min.js'); }}
    {{ HTML::script('bower/twbs-pagination/jquery.twbsPagination.min.js'); }}
    {{ HTML::script('bower/purl/purl.js'); }}
    {{ HTML::script('bower/select2/select2.js')}}
    {{ HTML::script('bower/select2/select2_locale_fr.js')}}

    {{-- Tout les scripts globaux sont ici --}}
    {{ HTML::script('js/helper.js'); }}
    @include('js.global_api')
    @include('js.global_select2')

    {{ HTML::script('js/page-global.js'); }}

    {{-- Les scripts spéciaux par page sont chargé dans cette section --}}
    @yield('pageScript')

</body>
</html>