<script type="text/javascript">

sylvanasSelect2 = function()
{
	// function for generate item select2 input
	// create input with type="hidden"
	// enjoy it

	//====================
	//FORMAT =============
	//====================

	//input display
	var formatQuality = function(item)
	{
		return item.id + ' - ' + item.libelle;
	};

	//input display
	var formatDisplayid = function(item)
	{
		if(item.icon)
			return "<img style='width: 24px; height: 24px;' class='img-rounded' src='{{URL::to('img/icons/')}}/"+item.icon+".png'>"+' '+item.text;
		else
			return item.id;
	};

	//===============
	//DATA FORMAT ===
	//===============

	//update format from the api
	var select2DataDisplayid = function(data)
	{
		var newData = [];

        $.each(data, function (index, value) 
        {
            newData.push(
            {
               id: value.id,
               icon: value.icon,
               text: String(value.id) + '-' + value.icon
            });
        });

        return newData;
	};

	//===============
	//SELECT2 =======
	//===============

	var createDisplayInfoSelect2 = function(input)
	{
		input.select2(
		{
			minimumInputLength: 2,
			ajax: 
			{
				url : '{{URL::route("api.items.displayinfo")}}',
				dataType: 'json',
				data: function(term){
					return { q: term }
				},
				results: function(data){
					var newData = select2DataDisplayid(data);
					return {results: newData};
				},
			},
			initSelection: function(element, callback)
			{
				if(element.val()==='')
					return;

				sylvanasApi.getItemDisplayId(null, {'q': element.val()}).done(function(data)
				{
					var newData = select2DataDisplayid(data);
					return callback(newData[0]);
				})
			},
			formatSelection : formatDisplayid,
			formatResult: formatDisplayid,
			escapeMarkup: function (m) { return m; },
		});
	};

	var createQualitySelect2 = function(input)
	{
		sylvanasApi.getItemQuality(null).done(function(data)
		{
			input.select2(
			{
				data:{results: data, text:'libelle'},
				formatSelection: formatQuality,
				formatResult: formatQuality
			});
		});
	};

	//============================
	//============================

	return {
		createQualitySelect2 : createQualitySelect2,
		createDisplayInfoSelect2 : createDisplayInfoSelect2,
	};

}();
</script>