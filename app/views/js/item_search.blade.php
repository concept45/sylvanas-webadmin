<script type="text/javascript">

(function($) {
$(document).ready(function()
{
	applyItemSearchFilter = function(filters, page, beforeCallBack)
	{
		sylvanasHelper.setGetParameters('page', page, 1);

		$.each(filters, function(index, element){
            sylvanasHelper.setGetParameters(element.name, element.value, '');
        });

		var params = $.merge([{ 'name':'page', 'value':page }], filters);

		return getItem(page, params, beforeCallBack);
	}

	//generate each input
	itemSearchGenerateDOMForm = function(inputs)
	{
		var domItemSearch = "";
		var itemsPerLine = 3;

		$.each(inputs, function(index, item)
		{
			//if we have the parameters in url
			var getParams = $.url().param(item.column_name);

			if(getParams === null || getParams === undefined)
				getParams ="";

			//new line
			if(index % itemsPerLine === 0)
				domItemSearch += '<div class="form-group row">';

			domItemSearch += '<div class="col-lg-4">';
			domItemSearch += '<div class="input-group">';

			if(item.libelle)
				domItemSearch += '<div class="input-group-addon">'+item.libelle+'</div>';
			else
				domItemSearch += '<div class="input-group-addon">'+item.column_name+'</div>';
			
			domItemSearch += '<input class="form-control" name="'+item.column_name+'" value="'+getParams+'" type="text">';
			domItemSearch += '</div>';
			domItemSearch += '</div>';

			if(index % itemsPerLine === 2)
				domItemSearch += '</div>';

			//when is the last
			if((inputs.length-1) === index)
			{
				/*if((index+1) % itemsPerLine === 0) //the next is on new line
					domItemSearch += '<div class="form-group row">';

				domItemSearch += '<div class="col-lg-4">';
				domItemSearch += '<div class="input-group">';
				domItemSearch += '<div class="input-group-addon">'+item+'</div>';
				domItemSearch += '<input class="form-control" type="text">';
				domItemSearch += '</div>';
				domItemSearch += '</div>';*/

				domItemSearch += '</div>';
			}
		});

		return domItemSearch;
	}

});
})(jQuery);

</script>