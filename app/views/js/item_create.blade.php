<script type="text/javascript">

//DOM Ready
(function($) {
$(document).ready(function()
{
	//===========================
	//CHECKBOX ===================

	generateDomItemAllowableClass = function(data)
	{
		//params
		var checkboxList = "";
		var itemsPerLine = 4;
		var allValue = -1;

		var paramName = 'AllowableClass';
		var paramValue = $.url().param(paramName);

		//all check
		checkboxList += '<div class="form-group">';
		checkboxList += '<label class="checkbox-inline">';
		checkboxList += '<input type="checkbox"'
		checkboxList += 'onchange="checkedCheckbox($(this), \''+paramName+'\', \''+paramName+'\', '+allValue+')"'
		checkboxList += 'value="'+allValue;
		(paramValue === allValue) ? checkboxList +='" checked>' : checkboxList +='">';
		checkboxList += 'Toutes les classes';
		checkboxList += '</label>';
		checkboxList += '</div>';

		$.each(data, function(index, item){

			var mask = 1 << (item.id- 1); //way for the mask

			//new line
			if(index % itemsPerLine === 0)
				checkboxList += '<div class="form-group">';

			checkboxList += '<label class="checkbox-inline">'
			checkboxList += '<input type="checkbox"';
			checkboxList += 'onchange="checkedCheckbox($(this), \''+paramName+'\', \''+paramName+'\', '+allValue+')"'
			checkboxList += 'value="'+mask+'"';

			//should be checked
			if(paramValue && (paramValue & mask) && (paramValue !== allValue))
				checkboxList += 'checked'

			checkboxList += '>';
			checkboxList += item.libelle;
			checkboxList += '</label>';

			if(index % itemsPerLine === (itemsPerLine-1))
				checkboxList += '</div>';

			//when is the last
			if((data.length-1) === index)
				checkboxList += '</div>';
		});

		return checkboxList;
	}

	generateDomItemAllowableRace = function(data)
	{
		//params
		var checkboxList = "";
		var itemsPerLine = 3;
		var allValue = -1;

		var paramName = 'AllowableRace';
		var paramValue = $.url().param(paramName);

		//all check
		checkboxList += '<div class="form-group">';
		checkboxList += '<label class="checkbox-inline">';
		checkboxList += '<input type="checkbox"'
		checkboxList += 'onchange="checkedCheckbox($(this), \''+paramName+'\', \''+paramName+'\', '+allValue+')"'
		checkboxList += 'value="'+allValue;
		(paramValue == allValue) ? checkboxList +='" checked>' : checkboxList +='">';
		checkboxList += 'Toutes les races';
		checkboxList += '</label>';
		checkboxList += '</div>';

		$.each(data, function(index, item){

			var mask = 1 << (item.id- 1); //way for the mask

			//new line
			if(index % itemsPerLine === 0)
				checkboxList += '<div class="form-group">';

			checkboxList += '<label class="checkbox-inline">'
			checkboxList += '<input type="checkbox"';
			checkboxList += 'onchange="checkedCheckbox($(this), \''+paramName+'\', \''+paramName+'\', '+allValue+')"'
			checkboxList += 'value="'+mask+'"';

			//should be checked
			if(paramValue && (paramValue & mask) && (paramValue !== allValue))
				checkboxList += 'checked'

			checkboxList += '>';
			checkboxList += item.libelle;
			checkboxList += '</label>';

			if(index % itemsPerLine === (itemsPerLine-1))
				checkboxList += '</div>';

			//when is the last
			if((data.length-1) === index)
				checkboxList += '</div>';
		});

		return checkboxList;
	}


	//============================
	//BUTTONS ====================
	
	generateDomItemSubclass = function(data)
	{
		var buttonList = "";
		var itemsPerLine = 6;
		var paramName = 'subclass';

		$.each(data, function(index, item){

			var classButton = 'btn btn-default btn-sm';

			//new line
			if(index % itemsPerLine === 0)
				buttonList += '<div class="form-group">';

			//first button is alway active
			if(index === 0)
				classButton = 'btn btn-primary btn-sm';

			buttonList += '<button class="'+classButton+'"'

			//onclick event
			buttonList += 'onclick="return clickOnSelectButtons(\''+paramName+'\', $(this), $(\'input[name='+paramName+']\'))"'
			
			//value and libelle
			buttonList += 'value="'+item.subclass+'">';
			buttonList += (item.libelle_subclass!="") ? item.libelle_subclass : item.libelle_class;
			
			buttonList +='</button> ';

			if(index % itemsPerLine === (itemsPerLine-1))
				buttonList += '</div>';

			//when is the last
			if((data.length-1) == index)
				buttonList += '</div>';
		});

		return buttonList;
	}

	generateDomItemClass = function(data)
	{
		var buttonList = "";
		var itemsPerLine = 8;

		$.each(data, function(index, itemClass){

			var classButton = 'btn btn-default btn-sm';

			//new line
			if(index % itemsPerLine === 0)
				buttonList += '<div class="form-group">';

			if(index === 0)
				classButton = 'btn btn-primary btn-sm';

			buttonList += '<button class="'+classButton+'" onclick="return clickOnClass($(this))" value="'+itemClass.id+'">'+itemClass.libelle+'</button> ';

			if(index % itemsPerLine === 7)
				buttonList += '</div>';

			//when is the last
			if((data.length-1) === index)
				buttonList += '</div>';
		});

		return buttonList;
	}

	generateDomItemInventorytype = function(data)
	{
		var buttonList = "";
		var itemsPerLine = 8;
		var paramName = 'InventoryType';

		$.each(data, function(index, item){

			var classButton = 'btn btn-default btn-sm';

			//new line
			if(index % itemsPerLine === 0)
				buttonList += '<div class="form-group">';

			//first button is alway active
			if(index === 0)
				classButton = 'btn btn-primary btn-sm';

			buttonList += '<button class="'+classButton+'"'
			
			//onclick event
			buttonList += 'onclick="return clickOnSelectButtons(\''+paramName+'\', $(this), $(\'input[name='+paramName+']\'))"'
			
			//value and libelle
			buttonList += 'value="'+item.id+'">';
			buttonList += item.libelle;
			
			buttonList +='</button> ';

			if(index % itemsPerLine === (itemsPerLine-1))
				buttonList += '</div>';

			//when is the last
			if((data.length-1) == index)
				buttonList += '</div>';
		});

		return buttonList;
	}
});
})(jQuery);

</script>