<script type='text/javascript'>

//API Function should be here
sylvanasApi = function()
{
    //==================
    //Item =============
    //================== 
    var getItem = function(page, filters, beforeCallBack){
        sylvanasHelper.setGetParameters('page', page, 1);

        if(filters !== null)
            var params = $.merge([{ 'name':'page', 'value':page }], filters);
        else
            var params = {'page': page};

        return sylvanasHelper.requestAjax("{{URL::route('api.items.list')}}", params, beforeCallBack);  
    };

    var getItemDisplayId = function(beforeCallBack, params){
        return sylvanasHelper.requestAjax("{{URL::route('api.items.displayinfo')}}", params, beforeCallBack);
    };

    var getItemClass = function(beforeCallBack){
        return sylvanasHelper.requestAjax("{{URL::route('api.items.class')}}", null, beforeCallBack);
    };

    var getItemSubclass = function(beforeCallBack, id){
        return sylvanasHelper.requestAjax("{{URL::route('api.items.subclass')}}/"+id, null, beforeCallBack);
    };

    var getItemSearchFilter = function(beforeCallBack) {
        return sylvanasHelper.requestAjax("{{URL::route('api.filters.item_template')}}", null, beforeCallBack);
    };

    var getItemQuality = function(beforeCallBack){
        return sylvanasHelper.requestAjax("{{URL::route('api.items.quality')}}", null, beforeCallBack);
    };

    var getItemInventorytype = function(beforeCallBack){
        return sylvanasHelper.requestAjax("{{URL::route('api.items.inventorytype')}}", null, beforeCallBack);
    };

    var getItemCount = function(){
        return sylvanasHelper.requestAjax("{{URL::route('api.items.count')}}", null, null);
    };

    var getItemQualityClass = function(quality){
        switch (quality) 
        {
            case 0: return 'item-quality-poor'; break;
            case 2: return 'item-quality-uncommon'; break;
            case 3: return 'item-quality-rare'; break;
            case 4: return 'item-quality-epic'; break;
            case 5: return 'item-quality-legendare'; break;
            case 6: return 'item-quality-artifact'; break;
            case 7: return 'item-quality-gold'; break;
            default : return 'item-quality-common'; break;
        }
    };

    //=====================
    //Characters ==========
    //=====================

    var getCharactersClasses = function(beforeCallBack){
        return sylvanasHelper.requestAjax("{{URL::route('api.characters.classes')}}", null, beforeCallBack);
    };

    var getCharactersRaces = function(beforeCallBack){
        return sylvanasHelper.requestAjax("{{URL::route('api.characters.races')}}", null, beforeCallBack);
    };
    
    //==================
    //Available function
    return { 

        //Item
        getItem : getItem,
        getItemSearchFilter : getItemSearchFilter,  
        getItemDisplayId : getItemDisplayId,
        getItemClass : getItemClass,
        getItemSubclass : getItemSubclass,
        getItemSearchFilter : getItemSearchFilter,
        getItemQuality : getItemQuality,
        getItemInventorytype : getItemInventorytype,
        getItemCount : getItemCount,
        getItemQualityClass : getItemQualityClass,

        //Characters
        getCharactersClasses: getCharactersClasses,
        getCharactersRaces: getCharactersClasses,
    };
}();


</script>