<script type='text/javascript'>

//DOM Ready
(function($) {

$(document).ready(function()
{
    var itemCount = $('#item_count');
    var customItemCount = $('#custom_item_count');


    //=============================
    //Function ====================
    //=============================

    getItemLine = function(id, displayinfo, displayid, name, locales,  quality, itemClass, level)
    {
        var tableLine = '<tr>';
        tableLine += '<td>'+id+'</td>';

        if(displayinfo && displayinfo.icon)
            tableLine += '<td><img src="{{URL::to('img/icons/')}}/'+displayinfo.icon+'.png" style="width: 42px; height: 42px;"" class="img-rounded" alt="'+displayinfo.id+'"></td>';
        else
             tableLine += '<td>'+displayid+'</td>';
        
        //Name classic
        tableLine += '<td class="'+sylvanasApi.getItemQualityClass(quality)+'">'+name+'</td>';
        if(locales !== null)
            tableLine += '<td class="'+sylvanasApi.getItemQualityClass(quality)+'">'+locales.name_loc2+'</td>';
        else
            tableLine += '<td class="'+sylvanasApi.getItemQualityClass(quality)+'"></td>';

        tableLine += '<td>';

        if(itemClass)
        {
            if( itemClass.libelle_subclass !== ''  && itemClass.libelle_subclass)
                tableLine += itemClass.libelle_subclass;
            else
                tableLine += itemClass.libelle_class;
        }
        
        tableLine += '</td>'; //look the table item_class
        tableLine += '<td class="text-right">'+level+'</td>';

        //action
        tableLine += '<td class="text-right"><div class="btn-group">'
        tableLine += '<button type="button" class="btn btn-primary btn-sm">Fiche</button> ';
        tableLine += '<button type="button" class="btn btn-warning btn-sm">Editer</button> ';
        tableLine += '<button type="button" class="btn btn-danger btn-sm">Supprimer</button> ';
        tableLine += '<button type="button" class="btn btn-default btn-sm"><i class="fa fa-plus-circle"</i></button>';
        tableLine += '</div></td>';
        tableLine += '</tr>';


        return tableLine;
    }

    //generate the table dom
    getDOMItemTable = function(items)
    {
        var tableLines = ''; //all table lines in HTML;
        
        $.each(items, function(index, item)
        {
            tableLines += getItemLine(item.entry, item.displayinfo, item.displayid, item.name, item.locales, item.Quality, item.itemClass, item.ItemLevel); //template of the line
        })
        return tableLines;
    }
});

})(jQuery);

</script>