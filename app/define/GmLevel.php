<?php 

namespace Define;

//Date from common.h
abstract class GmLevel
{
  
    const SEC_PLAYER         = 0;
    const SEC_MODERATOR      = 1;
    const SEC_GAMEMASTER     = 2;
    const SEC_ADMINISTRATOR  = 3;
    const SEC_CONSOLE        = 4;
}