<?php 

class LocalesItem extends Eloquent {

    protected $connection = 'mysql_world'; //connexion
    protected $table = 'locales_item'; //table
   
    protected $primaryKey  = 'entry'; //primarykey

     public function item()
     {
        return $this->hasOne('ItemTemplate', 'entry',  'entry');
     }
}