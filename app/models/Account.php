<?php


use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Account extends Eloquent implements UserInterface, RemindableInterface 
{
     use UserTrait, RemindableTrait;

     public  $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'account';

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->sha_pass_hash;
    }

    public function AccountAccess()
    {
        return $this->hasOne('AccountAccess', 'id');
    }

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('sha_pass_hash', 
                                'sessionkey', 
                                'v', 
                                's', 
                                'token_key', 
                                );

}
