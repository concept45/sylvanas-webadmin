<?php 

class ItemTemplate extends Eloquent {

    protected $connection = 'mysql_world'; //connexion
    protected $table = 'item_template'; //table
   
    protected $primaryKey  = 'entry'; //primarykey

     public function locales()
     {
        return $this->belongsTo('LocalesItem', 'entry', 'entry');
     }

     public function displayinfo()
     {
         return $this->belongsTo('ItemDisplayInfo', 'displayid', 'id');
     }
}