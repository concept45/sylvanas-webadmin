<?php

class Character_RacesController extends Controller 
{
	public function index()
	{
		$races = CharacterRaces::All();
		return Response::json($races);
	}
}