<?php

class Character_ClassesController extends Controller 
{
	public function index()
	{
		$classes = CharacterClasses::All();
		return Response::json($classes);
	}
}