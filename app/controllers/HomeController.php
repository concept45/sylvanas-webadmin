<?php

class HomeController extends BaseController 
{

	public function index()
	{
        $title = "Accueil";

        //ajax data sending
        if(Request::ajax())
        {
            $view = View::make('pages.home.partials.index')->render();

            return Response::json(array('html' => $view, 'page_title' => $title));
        }
         
		return View::make('pages.home.index', array('page_title' => $title));
	}
}
