<?php

class AuthController extends Controller {

    //display the login form
    public function login()
    {
        $title = 'Connexion';
        return View::make('pages.auth.login', array('page_title' => $title));
    }

    //login post
    public function loginPost()
    {
        $credentials = array(
          'username' => Input::get('username'),
          'password' => Input::get('password'),
        );

        $rememberMe = Input::get('remember-me');

       //good auth
       if(Auth::attempt($credentials, $rememberMe))
       {
          //check the level access
          if( $accountAccess = Auth::user()->accountAccess()->first())
          {
              if($accountAccess->gmlevel >= \Define\GmLevel::SEC_GAMEMASTER)
                  return Redirect::route('home.index');
          }

          //if level access is below
          Auth::logout();
          return  Redirect::route('auth.login')->with('error' , 'Niveau de compte insuffisant');
       }
       else
           return  Redirect::route('auth.login')->with('error' , 'Nom d\'utilisateur ou mot de passe invalide');
    }

    //logout url
    public function logout()
    {
        Auth::logout();
        return Redirect::route('auth.login')->with('success' , 'Déconnexion reussite');
    }
}
