<?php

class Item_DisplayInfoController extends Controller 
{
	public function index()
	{
		if($q = Input::get('q'))
		{
			$displayInfo = ItemDisplayInfo::where('id', 'LIKE', $q.'%')
											->orwhere('icon', 'like',  $q.'%')
											->take(100) //max result
											->get();
		}
		else
			$displayInfo = ItemDisplayInfo::All();

		return Response::json($displayInfo);
	}
}