<?php

class Item_SubclassController extends Controller 
{
	public function index($classid = null)
	{
		if(!isset($classid))
			$itemSubclass = ItemSubclass::All();
		else
		{
			$itemSubclass = ItemSubclass::Where("class", "=", $classid)->get();
		}

		return Response::json($itemSubclass);
	}
}