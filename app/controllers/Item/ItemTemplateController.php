<?php

class Item_TemplateController extends Controller 
{
     public function count()
     {
         //$count = ItemTemplate::all()->count(); //too much data !
         $count = DB::connection('mysql_world')->table('item_template')->count(); //better way
         return Response::json(array('count'=>$count));
     }

     //generate url like http://example.com/something?page=2
     public function index()
     {
        $inputs = Input::only('page', 'name', 'entry', 'Flags', 'quality', 'class', 'subclass', 'ScriptName', 'displayid');
        $items = ItemTemplate::with(array("locales", "displayinfo"));

        foreach ($inputs as $key => $value) 
        {
            if($key == "page")
                continue;

            if($value != "") 
            {
                $filtre = Filter_ItemTemplateController::cutFilter($value);

                //filter string
                if($key == "name")
                {
                   if($filtre['operator'] == "=")
                        $filtre['operator'] = "LIKE";
                   else if ($filtre['operator'] == "!=")
                        $filtre['operator'] = "NOT LIKE";
                }

                $items->where($key, $filtre['operator'], $filtre["value"]);
            }
        }

        $items = $items->paginate('50');
        
        //Eloquent n'aime pas les clé primaire composé, donc un ptit hack
        foreach($items as $item)
        {
            $item->itemClass = ItemSubclass::whereRaw('class = ? and subclass = ?', array($item->class, $item->subclass))->first();
        }

         return $items->toJson();
     }

}
