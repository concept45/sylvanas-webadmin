<?php

//api controller for item
class Filter_ItemTemplateController extends Controller 
{
     //génére des url du type http://example.com/something?page=2
     public function index()
     {
         $filters = FilterItemTemplate::all();
         return Response::json($filters);
     }


     //Retourne l'opérateur du champ
     public static function cutFilter($input)
     {
     	$result = array("operator" => "=", "value" => $input);

     	$operator_1 = substr($input, 0, 1);
     	switch($operator_1)
     	{
     		//tout les symboles suivant peuvent être suivie d'un égale =
     		case ">":
     		case "<":
     		case "!":
     			$operator_2 = substr($input, 1, 1);

     			if($operator_2 == "=") //le pred est bon (<= & >= & !=)
     			{
     				$result["operator"] = $operator_1 ."". $operator_2;
     				$result["value"] = substr($input, 2);
     			}
     			else
     			{
     				if($operator_1 != "!")
     					$result["operator"] = $operator_1;

     				$result["value"] = substr($input, 1);
     			}

     			break;
     				
     		case "=":
     			$result["value"] = substr($input, 1);
     			break;
     	}

     	//par default rien
          //on peut retourner le résultat
     	return $result;
     }
}
