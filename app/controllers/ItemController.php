<?php

class ItemController extends BaseController 
{

    public function index()
    {
        //ajax data sending
        $title = 'Objets';

        if(Request::ajax())
        {
            $view = View::make('pages.item.partials.index')->render();
            return Response::json(array('html' => $view, 'page_title' => $title));
        }
            

        return View::make('pages.item.index', array('page_title' => $title));
    }

    public function create()
    {
        $title = 'Nouveau objet';

        if(Request::ajax())
        {
            $view = View::make('pages.item.partials.index_create')->render();
            return Response::json(array('html' => $view, 'page_title' => $title));
        }
            

        return View::make('pages.item.index_create', array('page_title' => $title));
    }
}