<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//=======================
//AUTHENTIFICATION ======
//=======================

//login
Route::get('login', array('as' => 'auth.login', 'uses' => 'AuthController@login', 'before' => array('guest') )); //login seulement invité
Route::post('login', array('uses' => 'AuthController@loginPost', 'before' => array('csrf', 'guest') )); //post login pour invité et csrf check

//logout
Route::get('logout', array('as' => 'auth.logout', 'uses' => 'AuthController@logout', 'before' => array('auth') )); //logout for auth


//=================================

//Toutes les routes ci dessous doivent être auth
Route::group(array('before' => 'auth.gamemaster'), function()
{

    //========================
    //HOME ===================
    //========================
    Route::get('home', array('as' => 'home.index',  'uses' => 'HomeController@index'));


    //=========================
    //ITEM ====================
    //=========================
    Route::get('items', array('as' => 'item.index', 'uses' => 'ItemController@index'));
    Route::get('items/create', array('as' => 'item.index.create', 'uses' => 'ItemController@create'));
});

//========================
//API ====================
//========================

//utiliser du pluriel pour l'api est mieux

Route::group(array('prefix' => 'api'), function()
{
    //===================
    //ITEMS =============
    //===================
    Route::group(array('prefix' => 'items'), function()
    {
        Route::get('count', array('as' => 'api.items.count', 'uses' => 'Item_TemplateController@count'));
        Route::get('class', array('as' => 'api.items.class', 'uses' => 'Item_ClassController@index'));
        Route::get('subclass/{classid?}', array('as' => 'api.items.subclass', 'uses' => 'Item_SubclassController@index'));
        Route::get('displayinfo/{id?}', array('as' => 'api.items.displayinfo', 'uses' => 'Item_DisplayInfoController@index'));
        Route::get('quality', array('as' => 'api.items.quality', 'uses' => 'Item_QualityController@index'));
        Route::get('inventorytype', array('as' => 'api.items.inventorytype', 'uses' => 'Item_InventorytypeController@index'));
        Route::get('', array('as' => 'api.items.list', 'uses' => 'Item_TemplateController@index', 'before' => array('auth.gamemaster'))); 
    });

    //===================
    //CHARACTERS ========
    //===================
    Route::group(array('prefix' => 'characters'), function(){
         Route::get('classes', array('as' => 'api.characters.classes', 'uses' => 'Character_ClassesController@index'));
         Route::get('races', array('as' => 'api.characters.races', 'uses' => 'Character_RacesController@index'));
    });

    //===================
    //FILTERS ===========
    //=================== 
    Route::group(array('prefix' => 'filters'), function()
    {
        Route::get('item_template', array('as' => 'api.filters.item_template', 'uses' => 'Filter_ItemTemplateController@index'));
    });

});

//=========================
//404 Redirection =========
//=========================

App::missing(function($exception)
{
    //return Response::view('errors.missing', array(), 404);
    return Redirect::route('auth.login');
});


