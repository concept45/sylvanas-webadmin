## Sylvanas WebAdmin 3.3.5

Sylvanas est un outil d'administration et de développement pour TrinityCore 335

## Installation

### Les bases de données

Sylvanas s'articule autour de 4 base de données configuré dans le fichier

```
/app/config/database.php
```

Vous devez exécuter l'ensemble des sql fournis dans le dossier /sql sur les bases de données associé.

### Laravel - Framework PHP

L'application est construite autour du framework Laravel.
Je vous conseille donc d'installer tout les prérequis (composer, bower, nodejs) et de lancer la commande suivante :

```
composer update
```

## Commandes utiles

Pour voir toutes les routes de notre application

```
php artisan routes
```

Pour les includes

```
composer dump-autoload
```